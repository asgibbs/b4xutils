﻿Type=StaticCode
Version=5.51
ModulesStructureVersion=1
B4J=true
@EndOfDesignText@
Sub Process_Globals
	Private Dir As Directory
	Public AppHomeDir, DataDir As String
End Sub

Public Sub Initialize
	Dir.Initialize
	Dir.MakeRootDir("UFOProgrammer13")
	File.MakeDir(Dir.Append("UFOProgrammer13").ToString, "JarTools")
	AppHomeDir = Dir.Append("UFOProgrammer13").Append("JarTools").ToString
	File.MakeDir(AppHomeDir, "FilesTool")
	DataDir = Dir.Append(AppHomeDir).Append("FilesTool").ToString
	LogDebug($"App Home: ${AppHomeDir}"$)
End Sub