﻿Type=StaticCode
Version=5.45
ModulesStructureVersion=1
B4J=true
@EndOfDesignText@
Sub Process_Globals
	Private Dir As B4XDir
	Public RootDir, AppHomeDir As String
End Sub

Public Sub Initialize
	Dir.Initialize
	Dir.MakeRootDir("UFOProgrammer13")
	File.MakeDir(Dir.Append("UFOProgrammer13").ToString, "B4X-Utils")
	RootDir = Dir.Append("UFOProgrammer13").Append("B4X-Utils").ToString
	File.MakeDir(RootDir, "CopyAbmWwwFolder")
	AppHomeDir = Dir.Append("UFOProgrammer13").Append("B4X-Utils").Append("CopyAbmWwwFolder").ToString
End Sub